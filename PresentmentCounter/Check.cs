﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace PresentmentCounter
{
    class Check
    {
        public static string[] ForFiles(string pDirectory)
        {
            string[] files = new string[0];
            try
            {
                files = Directory.GetFiles(pDirectory).Select(file => Path.GetFileName(file)).ToArray(); //Get file names from directory
                for (int i = 0; i < files.Length; i++)
                {
                    if (files[i].Contains("T112")) //Get only T112 files
                    {
                        files[i] = pDirectory + "\\" + files[i]; //Format as directory/filename

                        //Get rid of files in the array that aren't from today
                        string fileDateTime = File.GetCreationTime(files[i]).ToString();
                        string fileDate = fileDateTime.Substring(0, fileDateTime.IndexOf(" ") + 1); //Get date of current file
                        string dateTime = DateTime.Today.ToString();
                        string date = dateTime.Substring(0, dateTime.IndexOf(" ") + 1); //Get current date

                        if (fileDate != date) //If file is not from the current day
                        {
                            files[i] = null; //Replace file in the array that isn't from today with null
                        }
                    }
                    else
                    {
                        files[i] = null; //Replace non T112 files with null
                    }     
                }
                if (files.Length != 0) //If array of files is not empty
                {
                    files = files.Where(x => !string.IsNullOrEmpty(x)).ToArray(); //Remove nulls from array
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception: " + e);
            }
            return files;
        }

        public static bool IsFileLocked(string pDirectory) // Check if text_fields_output.txt is still in use by File_Conv.exe
        {
            bool Locked = false;
            try
            {
                FileStream fs = File.Open(pDirectory, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None); //Try to open decodeLog
                fs.Close();
            }
            catch (IOException) //Exception thrown if file is in use 
            {
                return true;
            }
            return Locked;
        }
    }
}