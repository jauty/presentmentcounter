﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace PresentmentCounter
{
    class Program
    {
        static string fileDir = ConfigurationManager.AppSettings["fileDir"]; //Where the coded files are
        static string ipmDecodeDir = ConfigurationManager.AppSettings["ipmDecodeDir"]; //Where File_Conv.exe is
        static string decodeLog = ConfigurationManager.AppSettings["decodeLog"]; //Where the decodeLog will be written to
        static int closeAfter = int.Parse(ConfigurationManager.AppSettings["closeAfter"]); //Here incase AppD needs more time to read the string - set to 0 otherwise
        static void Main(string[] args)
        {
            try
            {
                bool createdMutex;
                using (var processMutex = new System.Threading.Mutex(false, "PresentmentCounter", out createdMutex)) {
                    if (!createdMutex)
                    {
                        Environment.Exit(0); //Close PresentmentCounter.exe
                    }  
                    else 
                    {
                        CleanUp(); //Cleanup any old files
                        string[] files = Check.ForFiles(fileDir); //Get list of files in fileDir
                            if (files.Length != 0) //If list of files is not 0 long - there are files to decode
                            {
                                for (int i = 0; i < files.Length; i++)
                                {
                                    Convert(files[i]); //Convert listed files
                                    CleanUp(); //Cleanup any remaining files
                                }
                            }      
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception: " + e);
            }
            Thread.Sleep(closeAfter); //Here incase AppD needs more time to read the string - set to 0 otherwise
            Environment.Exit(0); //Close PresentmentCounter.exe
        }

        static void CleanUp() //Cleanup old files
        {
            try
            {
                if (File.Exists(decodeLog) == true) //If decodeLog does exist
                {
                    string decodeLogDateTime = File.GetCreationTime(decodeLog).ToString();
                    string decodeLogDate = decodeLogDateTime.Substring(0, decodeLogDateTime.IndexOf(" ") + 1);
                    string dateTime = DateTime.Today.ToString();
                    string date = dateTime.Substring(0, dateTime.IndexOf(" ") + 1);

                    if (decodeLogDate != date) //Check if decode is old
                    {
                        File.Delete(decodeLog); //Delete decodeLog if old
                    }
                }
                if (File.Exists(decodeLog) == false) //If decodeLog doesn't exist
                {
                    StreamWriter sw = File.CreateText(decodeLog); //Create decodeLog txt file
                    sw.Close();
                }
                //Delete old decode files - should only be present if PresentmentCounter.exe closed without finishing
                File.Delete(ipmDecodeDir + "\\output.txt");
                File.Delete(ipmDecodeDir + "\\text_fields_output.txt");
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception: " + e);
            }
        }

        static void Convert(string pInputFile)
        {
            try
            {
                //Check if file has been decoded before
                Boolean decoded = false;
                using (FileStream fs = File.Open(decodeLog, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                using (BufferedStream bs = new BufferedStream(fs))
                using (StreamReader sr = new StreamReader(bs))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (line.Contains(pInputFile) == true)
                        {
                            decoded = true;
                        }
                    }
                }
                //Start decode command - wait for file_conv.exe to finish
                if (decoded == false) //If file hasn't been decoded before
                {
                    Process cmd = new Process(); //New cmd process
                    cmd = Process.Start("cmd.exe", "/K cd " + ipmDecodeDir + " && file_conv.exe ascii_to_ebcdic_main.cfg " + pInputFile + " output.txt 2>text_fields_output.txt"); //Run decode command
                    //Sleep until file_conv.exe is finished
                    while (File.Exists(ipmDecodeDir + "\\text_fields_output.txt") == false)
                    {
                        Thread.Sleep(1);
                    }
                    while (Check.IsFileLocked(ipmDecodeDir + "\\text_fields_output.txt") == true)
                    {
                        Thread.Sleep(1);
                    }
                    cmd.Kill(); //Kill cmd process
                    Count(pInputFile, ipmDecodeDir + "\\text_fields_output.txt"); //Count "MTID: 1240" in decoded file
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception: " + e);
            }
        }

        static void Count(string pInputFile, string pOutputFile)
        {
            try
            {
                //Cout "MTID: 1240" in pOutputFile
                int count = 0;
                using (FileStream fs = File.Open(pOutputFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                using (BufferedStream bs = new BufferedStream(fs))
                using (StreamReader sr = new StreamReader(bs))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (line.Contains("MTID: 1240") == true)
                        {
                            count++;
                        }
                    }
                }
                //Check if input file is "LPF" or "OFBF"
                if (pInputFile.Contains("LPF")) //If file is "LPF"
                {
                    Console.WriteLine("name=Custom Metrics|PresentmentFileCheck|LPF T112|Count, value=" + count); //Write AppD metric string to console
                }
                else if (pInputFile.Contains("OFBF")) //If file is "OFBF"
                {
                    Console.WriteLine("name=Custom Metrics|PresentmentFileCheck|OFBF T112|Count, value=" + count); //Write AppD metric string to console
                }
                File.AppendAllText(decodeLog, pInputFile + Environment.NewLine); //Update decodeLog - file has been decoded
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception: " + e);
            }
        }
    }
}